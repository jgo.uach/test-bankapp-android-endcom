package com.endcom.bankapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.endcom.bankapp.data.model.BalanceDummy
import com.endcom.bankapp.databinding.CardBalanceBinding

class BalancesAdapter(
    var list: List<BalanceDummy>
) : RecyclerView.Adapter<BalancesAdapter.ViewHolder>() {

    class ViewHolder(val binding: CardBalanceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: Any?) {
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CardBalanceBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val viewholder = ViewHolder(binding)

        return viewholder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvTitle.text = list[position].titulo
        holder.binding.tvAmmount.text = "$"+list[position].saldo.toString()
    }

    override fun getItemCount(): Int {
        return list.size
    }
}