package com.endcom.bankapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.RecyclerView
import com.endcom.bankapp.R
import com.endcom.bankapp.data.model.MovementModel
import com.endcom.bankapp.databinding.ItemMovementDetailsBinding

class MovementsAdapter(
    var list: List<MovementModel>,
    var showMoreMovements: Boolean = false
) : RecyclerView.Adapter<MovementsAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemMovementDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: Any?) {
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovementDetailsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val viewholder = ViewHolder(binding)

        return viewholder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.txtDesc.text = list[position].descripcion
        holder.binding.txtAmount.text = list[position].monto.toString()
        holder.binding.txtDate.text = list[position].fecha
        if (list[position].tipo == "abono")
            holder.binding.txtAmount.setTextColor(getColor(holder.binding.root.context, R.color.green))
        else
            holder.binding.txtAmount.setTextColor(getColor(holder.binding.root.context, R.color.red))
    }

    override fun getItemCount(): Int {
        return if (!showMoreMovements)
            3
        else
            list.size
    }

    fun showMore(show: Boolean){
        this.showMoreMovements = show
        notifyDataSetChanged()
    }
}