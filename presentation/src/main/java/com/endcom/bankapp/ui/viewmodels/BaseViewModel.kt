package com.endcom.bankapp.ui.viewmodels

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.endcom.bankapp.data.BaseFetchData
import com.endcom.bankapp.core.ServerRetrofit
import com.endcom.bankapp.data.ErrorStatus
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

open class BaseViewModel : ViewModel(){
    private var disposables = CompositeDisposable()
    protected var onErrorLiveData = MutableLiveData<String>()
    protected var onSuccessLiveData = MutableLiveData<Any>()
    protected var onLoaderLiveData = MutableLiveData<Boolean>()

    protected val _dataloading = MutableLiveData<Boolean>()
    val dataLoading : LiveData<Boolean> = _dataloading

    protected val _onError = MutableLiveData<ErrorStatus>()
    val onError : LiveData<ErrorStatus> = _onError

    var liveDataMerger: MediatorLiveData<*> = setupLiveDataMerge()

    protected val serverRetrofit = ServerRetrofit

    open fun setupLiveDataMerge(): MediatorLiveData<BaseFetchData> {
        val liveDataMerger = MediatorLiveData<BaseFetchData>()

        liveDataMerger.addSource(onErrorLiveData) {
            if (it != null) {
                liveDataMerger.value = BaseFetchData.OnError(it)
            }
        }

        liveDataMerger.addSource(onSuccessLiveData) {
            if (it != null) {
                liveDataMerger.value = BaseFetchData.OnSuccess(it)
            }
        }

        liveDataMerger.addSource(onLoaderLiveData) {
            if (it != null) {
                liveDataMerger.value = BaseFetchData.OnLoader(it)
            }
        }

        return liveDataMerger
    }

    protected open fun <T> setupSubscribe(
        observable: Flowable<T>,
        onSuccess: Consumer<T>,
        onError: Consumer<Throwable>? = null,
        onCompletable: Action? = null
    ) {
        disposables.add(
            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSuccess,
                    onError ?: io.reactivex.functions.Consumer {
                        Log.d(
                            "setupSubscribe",
                            "Error no emitido para el Flowable"
                        )
                    },
                    onCompletable ?: io.reactivex.functions.Action {
                        Log.d(
                            "setupSubscribe",
                            "completable no emitido para el Flowable"
                        )
                    })
        )
    }

    protected open fun <T> setupSubscribe(
        observable: Observable<T>,
        onSuccess: Consumer<T>,
        onError: Consumer<Throwable>? = null
    ) {
        disposables.add(
            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    onSuccess,
                    onError ?: io.reactivex.functions.Consumer {
                        Log.d(
                            "setupSubscribe",
                            "Error no emitido para el observable"
                        )
                    })
        )
    }

    protected open fun <T> setupSubscribe(
        observable: Maybe<T>,
        onSuccess: Consumer<T>,
        onError: Consumer<Throwable>? = null,
        onCompletable: Action? = null
    ) {
        disposables.add(
            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSuccess,
                    onError ?: io.reactivex.functions.Consumer {
                        Log.d(
                            "setupSubscribe",
                            "Error no emitido para el Maybe"
                        )
                    },
                    onCompletable ?: io.reactivex.functions.Action {
                        Log.d(
                            "setupSubscribe",
                            "Completable no emitido para el Maybe"
                        )
                    })
        )
    }

    protected open fun <T> setupSubscribe(
        observable: Single<T>,
        onSuccess: Consumer<T>,
        onError: Consumer<Throwable>? = null
    ) {
        disposables.add(
            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    onSuccess,
                    onError ?: io.reactivex.functions.Consumer {
                        Log.d(
                            "setupSubscribe",
                            "Error no emitido para el Single"
                        )
                    })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposables.clear()
    }

    companion object {
        fun <T : ViewModel> getViewModel(context: FragmentActivity, viewModel: Class<T>): T {
            return ViewModelProvider(context).get(viewModel)
        }

        fun <T : ViewModel> getViewModel(context: Fragment, viewModel: Class<T>): T {
            return ViewModelProvider(context).get(viewModel)
        }
    }
}