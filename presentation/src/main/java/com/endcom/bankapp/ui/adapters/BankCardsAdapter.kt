package com.endcom.bankapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.endcom.bankapp.R
import com.endcom.bankapp.data.model.BankCardModel
import com.endcom.bankapp.databinding.ItemBankcardBinding

class BankCardsAdapter(
    var list: List<BankCardModel>
) : RecyclerView.Adapter<BankCardsAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemBankcardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: Any?) {
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBankcardBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val viewholder = ViewHolder(binding)

        return viewholder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.txtStatus.text = list[position].estado
        holder.binding.txtNumber.text = list[position].tarjeta
        holder.binding.txtName.text = list[position].nombre
        holder.binding.txtBalance.text = list[position].saldo.toString()
        holder.binding.txtType.text = list[position].tipo

        if (list[position].estado == "activa")
            holder.binding.imgBankcard.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_bankcard_enabled))
        else
            holder.binding.imgBankcard.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, R.drawable.ic_bankcard_disabled))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}