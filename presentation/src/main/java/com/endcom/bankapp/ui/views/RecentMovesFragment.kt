package com.endcom.bankapp.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.endcom.bankapp.R
import com.endcom.bankapp.databinding.FragmentRecentMovementsBinding

class RecentMovesFragment : Fragment() {

    lateinit var binding: FragmentRecentMovementsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = setupInflatedView(inflater, container)

    private fun setupInflatedView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_recent_movements, container, false)
        binding.lifecycleOwner = this.viewLifecycleOwner

        return binding.root
    }
}