package com.endcom.bankapp.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.endcom.bankapp.R
import com.endcom.bankapp.data.BaseFetchData
import com.endcom.bankapp.data.model.*
import com.endcom.bankapp.databinding.FragmentMainBinding
import com.endcom.bankapp.ui.adapters.BalancesAdapter
import com.endcom.bankapp.ui.adapters.BankCardsAdapter
import com.endcom.bankapp.ui.adapters.MovementsAdapter
import com.endcom.bankapp.ui.viewmodels.BaseViewModel
import com.endcom.bankapp.ui.viewmodels.MainViewModel

class MainFragment: Fragment() {

    lateinit var viewModel: MainViewModel
    lateinit var binding: FragmentMainBinding
    var showDetails: Boolean = false
    lateinit var adapterMovements: MovementsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = setupInflatedView(inflater, container)

    private fun setupInflatedView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_main, container, false)
        binding.lifecycleOwner = this.viewLifecycleOwner

        binding.tvAddBankcard.setOnClickListener {
            findNavController().navigate(
                MainFragmentDirections.actionGoToForm()
            )
        }

        setupViewModel()

        binding.fragmentMovements.lyShow.setOnClickListener {
            showDetails = !showDetails
            adapterMovements.showMore(showDetails)
            adapterMovements.notifyDataSetChanged()
        }

        return binding.root
    }

    private fun setupViewModel(){
        viewModel = BaseViewModel.getViewModel(this, MainViewModel::class.java)
        viewModel.getAccounts()
        viewModel.getBalances()
        viewModel.getCards()
        viewModel.getMovements()

        viewModel.liveDataMerger.observe(viewLifecycleOwner, Observer {
            when (it) {
                is BaseFetchData.OnSuccess<*> -> {
                    when (it.data) {

                        is AccountModel -> {
                            binding.tvName.text = it.data.nombre
                            binding.tvLastUpdate.text = getString(R.string.label_main_lastupdate) + it.data.ultimaSesion
                        }

                        is BalanceModel -> {
                            val balancesModel = listOf(
                                BalanceDummy(getString(R.string.label_main_balance_example_title), it.data.saldoGeneral),
                                BalanceDummy("Total de ingreso", it.data.ingresos),
                                BalanceDummy("Total de gastos", it.data.gastos)
                            )

                            binding.rvBalances.adapter = BalancesAdapter(balancesModel)
                        }

                        is BankCardsModel -> {
                            binding.rvBankCards.adapter = BankCardsAdapter(it.data.tarjetas)
                        }

                        is MovementsModel -> {
                            adapterMovements = MovementsAdapter(it.data.movimientos, false)
                            binding.fragmentMovements.rvMovements.adapter = adapterMovements
                        }
                    }
                }
            }
        })
    }

}