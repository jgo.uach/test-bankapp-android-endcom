package com.endcom.bankapp.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.endcom.bankapp.data.services.BankappService
import io.reactivex.functions.Consumer

class MainViewModel : BaseViewModel() {

    private val service by lazy { serverRetrofit.getService(BankappService::class.java) }

    fun getAccounts(){
        setupSubscribe(service.getAccount(), {
            onSuccessLiveData.value = it.cuenta[0]
        }) {

        }
    }

    fun getBalances(){
        setupSubscribe(service.getBalance(), {
            onSuccessLiveData.value = it.saldos[0]
        }) {

        }
    }

    fun getCards(){
        setupSubscribe(service.getCards(), {
            onSuccessLiveData.value = it
        }) {

        }
    }

    fun getMovements(){
        setupSubscribe(service.getMovements(), {
            onSuccessLiveData.value = it
        }) {

        }
    }
}