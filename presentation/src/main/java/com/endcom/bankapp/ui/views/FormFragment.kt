package com.endcom.bankapp.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.endcom.bankapp.R
import com.endcom.bankapp.databinding.FragmentFormBinding
import org.json.JSONException
import org.json.JSONObject

class FormFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = setupInflatedView(inflater, container)

    lateinit var binding: FragmentFormBinding

    private fun setupInflatedView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_form, container, false)
        binding.lifecycleOwner = this.viewLifecycleOwner

        binding.btnAccept.setOnClickListener {
            showAlert()
        }

        return binding.root
    }

    fun showAlert(){
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Asociar Tarjeta")
        val json = JSONObject()
        try {
            json.put("numero", binding.etCardNumber.text)
            json.put("cuenta", binding.etAccount.text)
            json.put("issure", binding.etIssure.text)
            json.put("nombre", binding.etName.text)
            json.put("marca", binding.etBrand.text)
            json.put("estatus", binding.etStatus.text)
            json.put("saldo", binding.etBalance.text)
            json.put("tipo", binding.etType.text)
        } catch (e: JSONException) {
        }

        builder.setMessage(json.toString())
        val dialog = builder.create()
        dialog.show()
    }

}