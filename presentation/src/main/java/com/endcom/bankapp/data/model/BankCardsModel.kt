package com.endcom.bankapp.data.model

import com.google.gson.annotations.SerializedName

data class BankCardsModel(
    @SerializedName("tarjetas")
    val tarjetas: List<BankCardModel>
)

data class BankCardModel(
    @SerializedName("tarjeta")
    val tarjeta: String,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("saldo")
    val saldo: Double,
    @SerializedName("estado")
    val estado: String,
    @SerializedName("tipo")
    val tipo: String,
    @SerializedName("id")
    val id: Int
)