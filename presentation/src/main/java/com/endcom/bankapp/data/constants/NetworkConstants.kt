package com.endcom.bankapp.data.constants

object NetworkConstants {
    const val URL = "http://bankapp.endcom.mx/api/bankappTest/"
    const val ACCOUNTS_URL = "http://bankapp.endcom.mx/api/bankappTest/cuenta"
    const val BALANCES_URL = "http://bankapp.endcom.mx/api/bankappTest/saldos"
    const val CARDS_URL = "http://bankapp.endcom.mx/api/bankappTest/tarjetas"
    const val MOVEMENTS_URL = "http://bankapp.endcom.mx/api/bankappTest/movimientos"
}