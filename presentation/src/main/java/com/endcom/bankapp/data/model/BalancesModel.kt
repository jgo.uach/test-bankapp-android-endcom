package com.endcom.bankapp.data.model

import com.google.gson.annotations.SerializedName

data class BalancesModel(
    @SerializedName("saldos")
    val saldos: List<BalanceModel>
)

data class BalanceModel(
    @SerializedName("cuenta")
    val cuenta: Double,
    @SerializedName("saldoGeneral")
    val saldoGeneral: Double,
    @SerializedName("ingresos")
    val ingresos: Double,
    @SerializedName("gastos")
    val gastos: Double,
    @SerializedName("id")
    val id: Int
)

data class BalanceDummy(
    val titulo: String,
    val saldo: Double
)
