package com.endcom.bankapp.data.services

import com.endcom.bankapp.data.constants.NetworkConstants
import com.endcom.bankapp.data.model.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface BankappService {
    @GET
    fun getAccount(
        @Url url: String = NetworkConstants.ACCOUNTS_URL
    ): Observable<AccountsModel>

    @GET
    fun getBalance(
        @Url url: String = NetworkConstants.BALANCES_URL
    ): Observable<BalancesModel>

    @GET
    fun getCards(
        @Url url: String = NetworkConstants.CARDS_URL
    ): Observable<BankCardsModel>

    @GET
    fun getMovements(
        @Url url: String = NetworkConstants.MOVEMENTS_URL
    ): Observable<MovementsModel>
}