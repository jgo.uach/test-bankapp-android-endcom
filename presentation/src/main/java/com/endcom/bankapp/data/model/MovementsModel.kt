package com.endcom.bankapp.data.model

import com.google.gson.annotations.SerializedName

data class MovementsModel(
    @SerializedName("movimientos")
    val movimientos: List<MovementModel>
)

data class MovementModel(
    @SerializedName("fecha")
    val fecha: String,
    @SerializedName("descripcion")
    val descripcion: String,
    @SerializedName("monto")
    val monto: Double,
    @SerializedName("tipo")
    val tipo: String,
    @SerializedName("id")
    val id: Int
)