package com.endcom.bankapp.data.model

import com.google.gson.annotations.SerializedName

data class AccountsModel(
    @SerializedName("cuenta")
    val cuenta: List<AccountModel>
)

data class AccountModel(
    @SerializedName("cuenta")
    val cuenta: Double,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("ultimaSesion")
    val ultimaSesion: String,
    @SerializedName("id")
    val id: Int
)