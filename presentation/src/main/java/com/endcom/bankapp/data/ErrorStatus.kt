package com.endcom.bankapp.data

import java.lang.Exception

data class ErrorStatus(val message:String?=null, val code:Int?=null, val exception: Exception?=null)