package com.endcom.bankapp.data

import java.lang.Exception

sealed class BaseFetchData {
    class OnError(val message:String?):BaseFetchData()
    class OnSuccess<T>(val data:T?):BaseFetchData()
    class OnLoader(val show:Boolean=false):BaseFetchData()

    override fun toString(): String {
        return when (this) {
            is OnSuccess<*> -> "Success[data=$data]"
            is OnError -> "Error [Exception=$message]"
            is OnLoader -> "Loading"
            else -> {"Loading"}
        }
    }
}


sealed class Result<out R> {
    data class Success<out T>(val data:T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
    object Loading: Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error [Exception=$exception]"
            Loading -> "Loading"
            else -> {"Loading"}
        }
    }
}

val Result<*>.succeeded
    get() = this is Result.Success && data!=null